This python module needs two datasets to run.
1) Program parameters (minimum wage data). https://taxjusticenow.org/#/appendix -> Programs and Data for Replication
2) Distributional National Accounts data, Gabriel Zucman. http://gabriel-zucman.eu/usdina/ -> Distributional nation accounts micro files. (Note: The data was updated recently. To get Oct 2019 vintage, follow the relevant link.)

After downloading and unzipping the data, update conf_01.json with the appropriate paths and pass it as an argument to the command_line script.

The following blog posts will be helpful in understanding the data:
https://rpubs.com/kieroneil