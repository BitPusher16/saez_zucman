import sys
import os
import json
import stata_to_pandas.process

def parse_and_run():
    if os.path.isfile(sys.argv[1]):
        with open(sys.argv[1], 'r') as json_conf:
            conf = json.load(json_conf)
    stata_to_pandas.process.generate_plot(conf)

if __name__ == '__main__':
    parse_and_run()