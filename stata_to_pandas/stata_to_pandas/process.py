import os
import pandas as pd
from pandas._libs.lib import is_integer
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# https://stackoverflow.com/questions/45528029/python-how-to-create-weighted-quantiles-in-pandas
# Will return nans if weights are integer type and overflow.
def weighted_qcut(values, weights, q, **kwargs):
    # Return weighted quantile cuts from a given series, values.
    if is_integer(q):
        quantiles = np.linspace(0, 1, q + 1)
    else:
        quantiles = q
    order = weights.iloc[values.argsort()].cumsum()
    bins = pd.cut(order / order.iloc[-1], quantiles, **kwargs)
    return bins.sort_index()

def stats(df, sum_cols, note):
    print('----------\n' + note)
    print('len: ' + str(len(df.index)))
    print(df[sum_cols].sum())

def generate_plot(conf):

    # For debugging.
    pd.set_option("display.max_columns", None)
    pd.options.display.float_format = '{:,.2f}'.format

    year = conf['year']
    param_path = os.path.join(conf['book_dir'], 'Programs', 'parameters.csv')
    params = pd.read_csv(param_path, header=0)

    dina_path = os.path.join(conf['dina_dir'], 'usdina'+str(year)+'.dta')
    dina = pd.read_stata(dina_path)
    dina_available = list(dina)

    # Convert weights to float so we don't have overflow issues later.
    # Also, divide by 100,000 per notes here: https://rpubs.com/kieroneil/567393. (Gives pop=243M in 2018.)
    # (tax-by-gperc.do shows division by 10,000, but this appears to be outdated.)
    dina['dweght'] = dina['dweght'].astype('float64') / 100000
    stats(dina, ['dweght'], 'stats initial')

    # Possible error. The tax-by-gperc.do script (Oct 2019 vintage) wants min wage in current dollars,
    # but the min wage values in parameters.csv are not inflation adjusted.
    hours_worked_annual = 2087
    min_wage = hours_worked_annual * params[params['yr'] == year].iloc[0]['minwage']

    compo = ['salestax', 'proprestax', 'govcontrib', 'ditax', 'corptax', 'estatetax']
    ordering = ['salestax', 'proprestax', 'govcontrib', 'ditax', 'corptax', 'estatetax', 'denom', 'fninc', 'fikgi', 'hweal']
    using = ['peinc', 'fkequ', 'fkpen', 'fikgi', 'fiinc', 'fninc', 'princ', 'flemp', 'flmil', 'hweal', 'tax',
        'propbustax', 'dweght', 'dweghttaxu', 'id', 'married', 'second', 'female', 'old', 'oldexf', 'oldexm'] + compo

    dina = dina[using]
    dina['corptax'] = dina['corptax'] + dina['propbustax']

    # Skip special handling for top 400.
    dina['tax'] = dina[compo].sum(axis=1) # We are overwriting existing values in the tax column. Is this desired?
    dina['kg_agi'] = dina['fikgi']
    # Skip special handling for years prior to 1987.

    tot_kg_agi = np.average(dina['kg_agi'], weights=dina['dweght'])
    tot_ninc = np.average(dina['peinc'], weights=dina['dweght'])
    frac = tot_kg_agi / tot_ninc
    dina['pure_kg'] = max(0.0, frac-0.03) / frac * dina['kg_agi']
    # Skip special handling for 1986.
    dina['peinc_kg'] = dina['peinc'] + dina['pure_kg']


    stats(dina, ['dweght'], 'stats pre marriage adjustment')
    dina['married_codes'] = dina[['married']].apply(lambda p: p.cat.codes) # marr -> 1, sing -> 0
    cols_to_max = dict.fromkeys(['married_codes'], np.max)
    cols_to_avg = dict.fromkeys(['peinc', 'peinc_kg', 'hweal', 'fninc', 'fikgi', 'tax', 'dweght'] + compo, np.mean)
    cols_to_agg = {**cols_to_max, **cols_to_avg}
    dina_gb = dina.groupby(['id']).agg(cols_to_agg)
    dina_gb['second'] = dina_gb.apply(lambda r: 2 if r['married_codes'] == 1 else 1, axis=1)

    # "Expand" operation is a bit tricky with pandas.
    # https://stackoverflow.com/questions/49074021/repeat-rows-in-data-frame-n-times?noredirect=1&lq=1
    dina = dina_gb.loc[dina_gb.index.repeat(dina_gb['second'])].reset_index(drop=True)
    del dina['second']

    # Filter out those earning less than half of minwage.
    stats(dina, ['dweght'], 'stats pre drop minwage')
    count_pre = len(dina.index)
    # dina = dina[dina['peinc'] > (min_wage/2)]
    # dina = dina[dina['peinc'] > 0] # Use this line instead if we want to keep only earners who paid taxes.
    if conf['drop_half_min_wage'] == 1:
        dina = dina[dina['peinc'] > (min_wage/2)]
    count_post = len(dina.index)
    frac_dropped = (count_pre - count_post)/count_pre
    print('----------\nfraction dropped:' + str(frac_dropped))

    # Skip adjust top 400 in 2018.
    # Skip adjust top wealth 0.1% in 2018.
    dina['denom'] = dina['peinc_kg']

    # Do gperc.
    dina['percentile'] = weighted_qcut(dina['peinc'], dina['dweght'], conf['num_buckets'])

    # Skip add top 400 as separate row.

    # We have computed percentiles and are almost ready to produce the final table.
    # Variable name explanations taken from: https://rpubs.com/kieroneil/567393
    # Final columns in stata excel output are:
        # gperc: pretax income (peinc) percentile, we will group by this,
        # nb: number of individuals in this bucket,
        # thres: threshold, minimum pretax income to be in this percentile bucket,
        # sh: share of total nationwide taxes paid by this bucket,
        # avg: average income for this bucket,
        # tax: average taxes paid in this bucket,
        # effrate: effective tax rate = tax/denom,
        # salestax: ,
        # proprestax: ,
        # govcontrib: total contributions social insurance government,
        # ditax: taxes income wealth current (?),
        # corptax: corporate income tax,
        # estatetax: ,
        # denom: peinc_kg, which is pretax income plus pure capital gains,
        # fninc: income fiscal incl capgains,
        # fikgi: income fiscal capital gains,
        # hweal: total net wealth

    ws = lambda x: np.dot(x, dina.loc[x.index, 'dweght'])

    cols_to_agg = {
        'dweght': [np.sum],         # nb
        'peinc': [np.min, ws],      # thresh

        'salestax': [ws],
        'proprestax': [ws],
        'govcontrib': [ws],
        'ditax':[ws],
        'corptax':[ws],
        'estatetax':[ws],

        'peinc_kg':[ws]             # denom
    }

    ag = dina.groupby(['percentile']).agg(cols_to_agg).reset_index()
    ag.columns = ['_'.join(col).strip().strip('_') for col in ag.columns.values]

    ag.rename(columns={
        'dweght_sum': 'nb', 'peinc_amin':'thres', 'peinc_<lambda_0>':'avg', 'salestax_<lambda>':'salestax',
        'proprestax_<lambda>':'proprestax', 'govcontrib_<lambda>':'govcontrib', 'ditax_<lambda>':'ditax',
        'corptax_<lambda>':'corptax', 'estatetax_<lambda>':'estatetax', 'peinc_kg_<lambda>':'denom'
    }, inplace=True)

    # Before computing final statistics, prepare to subtract welfare payments from income of lowest 2 deciles, if directed.
    # Sources:
    # https://www.thebalance.com/welfare-programs-definition-and-list-3305759
    #     Six major welfare programs, TANF, Medicaid, SNAP, SSI, EITC, Housing Assistance.
    # https://fas.org/sgp/crs/misc/RL32760.pdf
    # https://www.acf.hhs.gov/sites/default/files/ofa/fy2018_tanf_moe_national_data_pie_chart_b508.pdf
    # https://www.acf.hhs.gov/sites/default/files/ofa/2018combined_totalrecipients_03252019_508.pdf
    #     TANF FY 2018, 3.2 million recipients, 1.1 million families, $31.3 billion.
    # https://www.kff.org/medicaid/issue-brief/medicaid-enrollment-spending-growth-fy-2018-2019/
    #     Medicaid, FY 2017, 73 million recipients, $557 billion.
    #     (Omit this. Saez and Zucman probably don't include taxes that Medicaid recipients paid on medical care.)
    # https://www.cbpp.org/research/food-assistance/policy-basics-the-supplemental-nutrition-assistance-program-snap
    # https://www.fns.usda.gov/pd/supplemental-nutrition-assistance-program-snap
    #     SNAP, 2018, 40.1 million recipients, $60.9 billion.
    # https://www.ssa.gov/policy/docs/statcomps/ssi_asr/2019/sect01.html#table2
    #     SSI, 2018 9.0 million recipients, $54,847,237,000
    # https://fas.org/sgp/crs/misc/R43805.pdf
    #     EITC, 2018, 26.5 million taxpayers, $64.9 billion.

    # $31.3B + $60.9B + $54.8B + $64.9B = $211.9B
    if(conf['subtract_welfare'] == 1):

        # https://stackoverflow.com/questions/62545426/filter-rows-from-a-pandas-colummn-binned-by-pandas-cut
        received_cash = pd.arrays.IntervalArray(ag['percentile']).overlaps(pd.Interval(0.0,conf['distribute_transfer_cash']))
        ag_received_cash = ag[received_cash]
        num_buckets_received_cash = len(ag_received_cash.index)
        all_income_received_cash = ag_received_cash['avg'].sum()
        all_income = ag['avg'].sum()
        all_income_received_cash_frac_of_whole = all_income_received_cash/all_income

        ag.loc[received_cash, 'avg'] += (211_000_000_000 / num_buckets_received_cash)

    # Before dividing weighted sums by nb, compute a few vars over the whole dataset.
    ag['tax'] = ag['salestax'] + ag['proprestax'] + ag['govcontrib'] + ag['ditax'] + ag['corptax'] + ag['estatetax']
    all_taxes_paid = ag['tax'].sum()
    ag['sh'] = ag['tax']/all_taxes_paid

    ag['avg'] = ag['avg']/ag['nb']
    ag['tax'] = ag['tax']/ag['nb']
    ag['effrate'] = ag['tax']/ag['avg']

    ag['salestax'] = ag['salestax']/ag['nb']/ag['avg']
    ag['proprestax'] = ag['proprestax']/ag['nb']/ag['avg']
    ag['govcontrib'] = ag['govcontrib']/ag['nb']/ag['avg']
    ag['ditax'] = ag['ditax']/ag['nb']/ag['avg']
    ag['corptax'] = ag['corptax']/ag['nb']/ag['avg']
    ag['estatetax'] = ag['estatetax']/ag['nb']/ag['avg']
    ag['denom'] = ag['denom']/ag['nb']/ag['avg']


    # Spot check against Saez, Zucman output.
    # Compared against "<book_dir>\SZ2019\SZ2019\TablesFigures\StataOutput\full2018.xlsx".
    # Confirmed that I am within 1% of authors' computed effective tax rate for all deciles.
    print(ag)


    # One last renaming to make char better.
    ag.rename(columns={
        'percentile': 'percentile', 'salestax':'sales_tax', 'proprestax':'property_tax', 'govcontrib':'social_insurance',
        'ditax':'income_tax', 'corptax':'corporate_tax', 'estatetax':'estate_tax'
    }, inplace=True)

    plot = ['percentile', 'sales_tax', 'property_tax', 'social_insurance', 'income_tax', 'corporate_tax', 'estate_tax']
    plottable = ag[plot]


    # Plot chart.
    ax = plottable.plot.bar(x='percentile', stacked=True)
    fsize = 18
    # https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    ax.legend(loc='upper left', bbox_to_anchor=(1.04,1), borderaxespad=0, fontsize=fsize)
    # ax.xaxis.get_label().set_fontsize(16)
    # ax.yaxis.get_label().set_fontsize(16)
    ax.set_xlabel('Income Bucket', fontsize=fsize)
    ax.tick_params('x', labelrotation=45)
    ax.set_ylabel('Effective Tax Rate', fontsize=fsize)

    title_str = '2018 Effective Tax Rates by Decile'
    if conf['drop_half_min_wage'] == 1: title_str += ', Under Half Min Wage Dropped'
    elif conf['drop_half_min_wage'] == 0: title_str += ', Under Half Min Wage Kept'
    if conf['subtract_welfare'] == 1: title_str += ', Transfers Subtracted'
    ax.set_title(title_str, fontsize=fsize)
    ax.yaxis.set_major_formatter(mpl.ticker.PercentFormatter(1.0))
    plt.tick_params(axis='both', which='major', labelsize=fsize)
    plt.subplots_adjust(bottom=0.2, right=0.8)
    plt.show()

    done = 1
    pass